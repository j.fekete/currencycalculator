package com.example.currencycalc;

public class Currency {
    public String currency_code;

    public   double median_rate;

    public Currency(String currency_code, double median_rate) {
        this.currency_code = currency_code;
        this.median_rate = median_rate;
    }

    public String getCurrencyCode() {
        return currency_code;
    }

    public double  getMedianRate() {
        return median_rate;
    }

    @Override
    public String toString(){
        return getCurrencyCode();
    }
}
