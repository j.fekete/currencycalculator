package com.example.currencycalc;

import androidx.appcompat.app.AppCompatActivity;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.tvResult)
    TextView Result;

    @BindView(R.id.bCalculate)
    Button Calculate;

    @BindView(R.id.etInput)
    EditText Input;

    @BindView(R.id.spFrom)
    Spinner FromCurrency;

    @BindView(R.id.spTo)
    Spinner ToCurrency;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        FromCurrency.setOnItemSelectedListener(this);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://hnbex.eu/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        RatesAPI ratesapi = retrofit.create(RatesAPI.class);
        Call<List<Currency>> call = ratesapi.getCurrency();

        call.enqueue(new Callback<List<Currency>>() {
            @Override
            public void onResponse(Call<List<Currency>> call, Response<List<Currency>> response)  {
                if(!response.isSuccessful()){
                    Result.setText("Code:"+response.code());
                    return;
                }
                List<Currency> currencys = response.body();
                for (Currency currency:currencys){
                    String[] items = new String[currencys.size()];
                    for(int i=0; i<currencys.size(); i++){

                        items[i] = currencys.get(i).getCurrencyCode();
                        Calculate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                double input;
                                String from;
                                String to;
                                double result;
                                double median_rate=1;
                                input = Double.parseDouble(Input.getText().toString());
                                from = FromCurrency.getSelectedItem().toString();
                                to = ToCurrency.getSelectedItem().toString();

                               if (from.equals("AUD")){
                                   if (to.equals("AUD")){ result=input*4.602544/4.602544;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("CAD")){ result=input*4.602544/5.142879;
                                   Result.setText(String.valueOf(result)); }
                               else if (to.equals("CZK")){ result=input*4.602544/0.299078;
                                   Result.setText(String.valueOf(result)); }
                               else if (to.equals("DKK")){ result=input*4.602544/0.997259;
                                   Result.setText(String.valueOf(result)); }
                               else if (to.equals("HUF")){ result=input*4.602544/2.200100;
                                   Result.setText(String.valueOf(result)); }
                               else if (to.equals("NOK")){ result=input*4.602544/0.740781;
                                        Result.setText(String.valueOf(result)); }
                               else if (to.equals("JPY")){ result=input*4.602544/6.207529;
                                         Result.setText(String.valueOf(result)); }
                               else if (to.equals("SEK")){ result=input*4.602544/0.709432;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("CHF")){ result=input*4.602544/7.001990;
                                        Result.setText(String.valueOf(result)); }
                               else if (to.equals("GBP")){ result=input*4.602544/8.860307;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("USD")){ result=input*4.602544/6.826235;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("BAM")){ result=input*4.602544/3.809901;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("EUR")){ result=input*4.602544/7.451518;
                                       Result.setText(String.valueOf(result)); }
                               else if (to.equals("PLN")){ result=input*4.602544/1.749840;
                                       Result.setText(String.valueOf(result)); }
                               }

                               if (from.equals("CAD")){
                                    if (to.equals("AUD")){ result=input*5.142879/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*5.142879/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*5.142879/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*5.142879/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*5.142879/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*5.142879/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*5.142879/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*5.142879/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*5.142879/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*5.142879/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*5.142879/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*5.142879/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*5.142879/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*5.142879/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                               if (from.equals("CZK")){
                                    if (to.equals("AUD")){ result=input*0.299078/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*0.299078/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*0.299078/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*0.299078/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*0.299078/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*0.299078/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*0.299078/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*0.299078/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*0.299078/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*0.299078/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*0.299078/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*0.299078/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*0.299078/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*0.299078/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                               if (from.equals("DKK")){
                                    if (to.equals("AUD")){ result=input*0.997259/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*0.997259/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*0.997259/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*0.997259/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*0.997259/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*0.997259/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*0.997259/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*0.997259/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*0.997259/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*0.997259/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*0.997259/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*0.997259/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*0.997259/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*0.997259/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                               if (from.equals("HUF")){
                                    if (to.equals("AUD")){ result=input*2.200100/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*2.200100/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*2.200100/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*2.200100/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*2.200100/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*2.200100/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*2.200100/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*2.200100/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*2.200100/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*2.200100/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*2.200100/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*2.200100/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*2.200100/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*2.200100/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                               if (from.equals("NOK")){
                                    if (to.equals("AUD")){ result=input*0.740781/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*0.740781/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*0.740781/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*0.740781/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*0.740781/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*0.740781/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*0.740781/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*0.740781/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*0.740781/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*0.740781/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*0.740781/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*0.740781/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*0.740781/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*0.740781/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("JPY")){
                                    if (to.equals("AUD")){ result=input*6.207529/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*6.207529/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*6.207529/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*6.207529/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*6.207529/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*6.207529/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*6.207529/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*6.207529/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*6.207529/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*6.207529/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*6.207529/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*6.207529/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*6.207529/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*6.207529/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("SEK")){
                                    if (to.equals("AUD")){ result=input*0.709432/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*0.709432/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*0.709432/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*0.709432/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*0.709432/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*0.709432/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*0.709432/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*0.709432/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*0.709432/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*0.709432/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*0.709432/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*0.709432/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*0.709432/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*0.709432/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("CHF")){
                                    if (to.equals("AUD")){ result=input*7.001990/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*7.001990/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*7.001990/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*7.001990/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*7.001990/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*7.001990/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*7.001990/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*7.001990/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*7.001990/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*7.001990/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*7.001990/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*7.001990/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*7.001990/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*7.001990/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("GBP")){
                                    if (to.equals("AUD")){ result=input*8.860307/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*8.860307/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*8.860307/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*8.860307/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*8.860307/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*8.860307/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*8.860307/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*8.860307/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*8.860307/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*8.860307/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*8.860307/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*8.860307/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*8.860307/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*8.860307/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("USD")){
                                    if (to.equals("AUD")){ result=input*6.826235/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*6.826235/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*6.826235/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*6.826235/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*6.826235/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*6.826235/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*6.826235/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*6.826235/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*6.826235/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*6.826235/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*6.826235/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*6.826235/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*6.826235/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*6.826235/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("BAM")){
                                    if (to.equals("AUD")){ result=input*3.809901/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*3.809901/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*3.809901/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*3.809901/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*3.809901/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*3.809901/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*3.809901/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*3.809901/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*3.809901/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*3.809901/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*3.809901/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*3.809901/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*3.809901/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*3.809901/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("EUR")){
                                    if (to.equals("AUD")){ result=input*7.451518/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*7.451518/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*7.451518/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*7.451518/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*7.451518/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*7.451518/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*7.451518/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*7.451518/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*7.451518/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*7.451518/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*7.451518/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*7.451518/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*7.451518/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*7.451518/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }

                                if (from.equals("PLN")){
                                    if (to.equals("AUD")){ result=input*1.749840/4.602544;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CAD")){ result=input*1.749840/5.142879;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CZK")){ result=input*1.749840/0.299078;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("DKK")){ result=input*1.749840/0.997259;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("HUF")){ result=input*1.749840/2.200100;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("NOK")){ result=input*1.749840/0.740781;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("JPY")){ result=input*1.749840/6.207529;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("SEK")){ result=input*1.749840/0.709432;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("CHF")){ result=input*1.749840/7.001990;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("GBP")){ result=input*1.749840/8.860307;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("USD")){ result=input*1.749840/6.826235;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("BAM")){ result=input*1.749840/3.809901;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("EUR")){ result=input*1.749840/7.451518;
                                        Result.setText(String.valueOf(result)); }
                                    else if (to.equals("PLN")){ result=input*1.749840/1.749840;
                                        Result.setText(String.valueOf(result)); }
                                }


                                //   Result.setText(String.valueOf(result));
                                Log.e("error", String.valueOf(to));
                                Log.e("error", String.valueOf(from));
                            }
                        });
                    }

                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, items);
                    FromCurrency.setAdapter(adapter);
                    ToCurrency.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Currency>> call, Throwable t) {
                Result.setText(t.getMessage());
            }
        });

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
