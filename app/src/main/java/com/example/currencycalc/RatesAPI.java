package com.example.currencycalc;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RatesAPI {

    @GET("api/v1/rates/daily/")
    Call<List<Currency>> getCurrency();
}
